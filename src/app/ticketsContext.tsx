import produce from "immer";
import React, { useEffect, useReducer } from "react";
import { firstValueFrom } from "rxjs";
import { BackendService, Ticket, User } from "../backend";

interface TicketsContextProviderProps {
  backend: BackendService;
  children: React.ReactNode;
}

export interface ITicketsContext {
  state: State;
  dispatch: React.Dispatch<Action>;
  addTicket: (description: string) => Promise<void>;
}

interface State {
  isSubmitting: boolean;
  isLoading: boolean;
  users: User[];
  allTickets: Ticket[];
  filteredTickets: TicketWithUser[];
}

type TicketFilterOptions = "COMPLETED" | "NOT_COMPLETED" | "ALL";

export type Action =
  | {
      type: "LOAD";
      payload: {
        tickets: Ticket[];
        users: User[];
      };
    }
  | {
      type: "SUBMITTING";
    }
  | {
      type: "ADD_TICKET";
      payload: Ticket;
    }
  | {
      type: "FILTER_TICKETS";
      payload: {
        ticketFilterOption: TicketFilterOptions;
      };
    };

export type TicketWithUser = Ticket & { assigneeName: string };

const initialState: State = {
  isSubmitting: false,
  isLoading: true,
  users: [],
  allTickets: [],
  filteredTickets: [],
};

function mapTicketsToTicketsWithUser(tickets: Ticket[], users: User[]) {
  return tickets.map((t) => {
    const user = users.find((u) => u.id === t.assigneeId);

    return {
      ...t,
      assigneeName: user?.name ?? "",
    };
  });
}

function computeFilteredTickets(
  allTickets: Ticket[],
  ticketFilterOption: TicketFilterOptions,
  users: User[]
): TicketWithUser[] {
  switch (ticketFilterOption) {
    case "ALL":
      return mapTicketsToTicketsWithUser(allTickets, users);
    case "COMPLETED":
      return mapTicketsToTicketsWithUser(
        allTickets.filter((t) => t.completed),
        users
      );
    case "NOT_COMPLETED":
      return mapTicketsToTicketsWithUser(
        allTickets.filter((t) => !t.completed),
        users
      );
    default:
      return mapTicketsToTicketsWithUser(allTickets, users);
  }
}

export default function ticketsReducer(state: State, action: Action): State {
  switch (action.type) {
    case "LOAD": {
      const nextState = produce<State, State>(state, (draftState) => {
        draftState.allTickets = action.payload.tickets;
        draftState.filteredTickets = computeFilteredTickets(
          action.payload.tickets,
          "ALL",
          action.payload.users
        );
        draftState.isLoading = false;
        draftState.users = action.payload.users;
      });

      return nextState;
    }
    case "ADD_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        const tickets = [...state.allTickets, action.payload];
        draftState.allTickets = tickets;
        draftState.filteredTickets = computeFilteredTickets(
          tickets,
          "ALL",
          state.users
        );
        draftState.isSubmitting = false;
      });

      return nextState;
    }
    case "FILTER_TICKETS": {
      const nextState = produce<State, State>(state, (draftState) => {
        draftState.filteredTickets = computeFilteredTickets(
          state.allTickets,
          action.payload.ticketFilterOption,
          state.users
        );
      });

      return nextState;
    }
    case "SUBMITTING": {
      const nextState = produce<State, State>(state, (draftState) => {
        draftState.isSubmitting = true;
      });
      return nextState;
    }
    default:
      return state;
  }
}

export const TicketsContext = React.createContext<ITicketsContext | null>(null);

export function TicketsContextProvider(props: TicketsContextProviderProps) {
  const { children, backend } = props;
  const [state, dispatch] = useReducer<React.Reducer<State, Action>, State>(
    ticketsReducer,
    initialState,
    () => {
      return initialState;
    }
  );

  useEffect(() => {
    let didCancel = false;
    if (state.isLoading) {
      const fetchData = async () => {
        const tickets = await firstValueFrom(backend.tickets());
        const users = await firstValueFrom(backend.users());
        if (!didCancel) {
          dispatch({
            type: "LOAD",
            payload: {
              tickets,
              users,
            },
          });
        }
      };
      fetchData();
    }
    return () => {
      didCancel = true;
    };
  }, [backend, state.isLoading]);

  const addTicket = async (description: string) => {
    dispatch({ type: "SUBMITTING" });
    const newTicket = await firstValueFrom(backend.newTicket({ description }));
    dispatch({ type: "ADD_TICKET", payload: newTicket });
  };

  const ticketsContextValue: ITicketsContext = {
    state,
    dispatch,
    addTicket,
  };

  return (
    <TicketsContext.Provider value={ticketsContextValue}>
      {children}
    </TicketsContext.Provider>
  );
}

export function useTicketsContext(): ITicketsContext {
  const ticketsContext = React.useContext(TicketsContext);

  if (!ticketsContext) {
    throw new Error(
      "TicketsContext does not have a value, it might not be inside a provider"
    );
  }

  return ticketsContext;
}
