import produce from "immer";
import React, { useEffect, useReducer } from "react";
import { useParams } from "react-router-dom";
import { firstValueFrom } from "rxjs";
import { BackendService, Ticket, User } from "../backend";
import { TicketWithUser } from "./ticketsContext";

interface TicketsContextProviderProps {
  backend: BackendService;
  children: React.ReactNode;
}

interface ITicketContext {
  state: State;
  dispatch: React.Dispatch<Action>;
  completeTicket: () => Promise<void>;
  assignTicket: (userId: number) => Promise<void>;
}

interface State {
  isLoading: boolean;
  isCompleting: boolean;
  isAssigning: boolean;
  ticket: TicketWithUser | null;
  users: User[];
}

export type Action =
  | {
      type: "COMPLETE_TICKET";
    }
  | {
      type: "COMPLETING_TICKET";
    }
  | {
      type: "ASSIGNING_TICKET";
    }
  | {
      type: "ASSIGN_TICKET";
      payload: {
        userId: number;
      };
    }
  | {
      type: "LOAD_TICKET";
      payload: {
        ticket: Ticket;
        users: User[];
      };
    };

const initialState: State = {
  isLoading: true,
  isAssigning: false,
  isCompleting: false,
  ticket: null,
  users: [],
};

function ticketReducer(state: State, action: Action): State {
  switch (action.type) {
    case "LOAD_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        const { ticket, users } = action.payload;
        draftState.ticket = {
          ...ticket,
          assigneeName:
            users?.find((u) => u.id === ticket.assigneeId)?.name ?? "",
        };
        draftState.users = action.payload.users;
        draftState.isLoading = false;
      });

      return nextState;
    }
    case "COMPLETE_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        if (draftState.ticket) {
          draftState.ticket.completed = true;
          draftState.isCompleting = false;
        }
      });
      return nextState;
    }
    case "COMPLETING_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        draftState.isCompleting = true;
      });
      return nextState;
    }
    case "ASSIGNING_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        draftState.isAssigning = true;
      });
      return nextState;
    }
    case "ASSIGN_TICKET": {
      const nextState = produce<State, State>(state, (draftState) => {
        if (draftState.ticket) {
          draftState.ticket.assigneeId = action.payload.userId;
          draftState.ticket.assigneeName =
            state.users?.find((u) => u.id === action.payload.userId)?.name ??
            "";
          draftState.isAssigning = false;
        }
      });
      return nextState;
    }
    default:
      return state;
  }
}

export const TicketContext = React.createContext<ITicketContext | null>(null);

export function TicketContextProvider(props: TicketsContextProviderProps) {
  const { children, backend } = props;
  const [state, dispatch] = useReducer<React.Reducer<State, Action>, State>(
    ticketReducer,
    initialState,
    () => {
      return initialState;
    }
  );
  const { ticketId } = useParams<{ ticketId: string }>();

  console.log({ ticketId });

  useEffect(() => {
    let didCancel = false;
    if (state.isLoading && ticketId) {
      const fetchData = async () => {
        const ticket = await firstValueFrom(backend.ticket(parseInt(ticketId)));
        const users = await firstValueFrom(backend.users());
        if (!didCancel) {
          dispatch({
            type: "LOAD_TICKET",
            payload: {
              ticket,
              users,
            },
          });
        }
      };
      fetchData();
    }
    return () => {
      didCancel = true;
    };
  }, [backend, state.isLoading, ticketId]);

  const assignTicket = async (userId: number) => {
    if (state.ticket) {
      dispatch({ type: "ASSIGNING_TICKET" });
      await backend.assign(state.ticket?.id, userId);
      dispatch({
        type: "ASSIGN_TICKET",
        payload: {
          userId,
        },
      });
    }
  };

  const completeTicket = async () => {
    if (state.ticket) {
      dispatch({ type: "COMPLETING_TICKET" });
      await backend.complete(state.ticket.id, true);
      dispatch({ type: "COMPLETE_TICKET" });
    }
  };

  const ticketContextValue = {
    state,
    dispatch,
    assignTicket,
    completeTicket,
  };

  return (
    <TicketContext.Provider value={ticketContextValue}>
      {children}
    </TicketContext.Provider>
  );
}

export function useTicketContext(): ITicketContext {
  const ticketContext = React.useContext(TicketContext);

  if (!ticketContext) {
    throw new Error(
      "TicketContext does not have a value, it might not be inside a provider"
    );
  }

  return ticketContext;
}
