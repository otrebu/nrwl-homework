import { Link } from "react-router-dom";
import AddTicket from "./addTicket";
import "./app.css";
import { useTicketsContext } from "./ticketsContext";

const Tickets = () => {
  const { state, dispatch } = useTicketsContext();
  const { filteredTickets, isLoading } = state;
  return (
    <div>
      <h2>Tickets</h2>

      {isLoading ? (
        <p>Loading...</p>
      ) : (
        <>
          <label>Status:</label>
          <p>
            <select
              onChange={(e) => {
                dispatch({
                  type: "FILTER_TICKETS",
                  payload: {
                    // @ts-expect-error y
                    ticketFilterOption: e.target.value,
                  },
                });
              }}
            >
              <option value="ALL">Any</option>
              <option value="COMPLETED">Completed</option>
              <option value="NOT_COMPLETED">In Progress</option>
            </select>
          </p>

          <table>
            <thead>
              <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Assignee</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {filteredTickets.map((t) => (
                <tr key={t.id}>
                  <td>
                    <Link to={`/tickets/${t.id}`}>{t.id} </Link>
                  </td>
                  <td>{t.description}</td>
                  <td>{t.assigneeName}</td>
                  <td>{t.completed ? "Completed" : "In Progress"}</td>
                </tr>
              ))}
            </tbody>
          </table>

          <AddTicket />
        </>
      )}
    </div>
  );
};

export default Tickets;
