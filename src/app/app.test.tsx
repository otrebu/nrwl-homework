import {
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import React from "react";
import { BackendService } from "../backend";
import App from "./app";

test("renders learn react link", () => {
  const backend = new BackendService();
  const { getByText } = render(<App backend={backend} />);
  const header = getByText(/Tickets/i);
  expect(header).toBeInTheDocument();
});

test("renders tickets", async () => {
  const backend = new BackendService();
  render(<App backend={backend} />);
  await waitForElementToBeRemoved(() => screen.getByText(/Loading.../i), {
    timeout: 500000,
  });
  const tickets = await screen.findAllByText(/Victor/i);
  expect(tickets).toHaveLength(2);
});
