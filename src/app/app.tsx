import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { BackendService } from "../backend";
import "./app.css";
import { TicketContextProvider } from "./ticketContext";
import TicketDetails from "./ticketDetails";
import Tickets from "./tickets";
import { TicketsContextProvider } from "./ticketsContext";

interface AppProps {
  backend: BackendService;
}

const App = ({ backend }: AppProps) => {
  return (
    <Router>
      <Switch>
        <Route path="/tickets/:ticketId">
          <TicketContextProvider backend={backend}>
            <TicketDetails />
          </TicketContextProvider>
        </Route>
        <Route exact path="/">
          <TicketsContextProvider backend={backend}>
            <Tickets />
          </TicketsContextProvider>
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
