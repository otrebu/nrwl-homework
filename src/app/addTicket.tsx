import React, { useEffect, useState } from "react";
import { useTicketsContext } from "./ticketsContext";

export default function AddTicket() {
  const { addTicket, state } = useTicketsContext();
  const [description, setDescription] = useState("");
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    if (description.length > 0) {
      setError("");
    }
  }, [description]);

  return (
    <form>
      <h3>Add ticket</h3>
      <p>
        <label>Description:</label>
        <br />
        <input
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        ></input>
      </p>
      {error ? <p style={{ color: "red" }}>{error}</p> : null}

      <button
        disabled={state.isSubmitting}
        onClick={async (e) => {
          e.preventDefault();
          e.stopPropagation();
          if (description.length > 0) {
            await addTicket(description);
            setDescription("");
          } else {
            setError("enter a description please");
          }
        }}
      >
        Add
      </button>
    </form>
  );
}
