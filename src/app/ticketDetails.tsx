import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useTicketContext } from "./ticketContext";

const TicketDetails = () => {
  const { state, completeTicket, assignTicket } = useTicketContext();
  const { ticket, isLoading, users, isCompleting, isAssigning } = state;
  const [selectedUserId, setSelectedUserId] = useState(
    ticket?.assigneeId ?? ""
  );
  const [message, setMessage] = useState<string | null>(null);

  useEffect(() => {
    let timeoutId: NodeJS.Timeout | null = null;
    if (message) {
      timeoutId = setTimeout(() => {
        setMessage(null);
      }, 2000);
    }
    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, [message]);

  return (
    <div>
      <nav>
        <Link to="/tickets">Tickets</Link>
      </nav>
      <h2>Ticket</h2>
      {isLoading && !ticket ? (
        <p>Loading...</p>
      ) : (
        <>
          <h4>Id:</h4>
          <p>{ticket?.id}</p>
          <h4>Description:</h4>
          <p>{ticket?.description}</p>
          <h4>Status:</h4>
          <p>{ticket?.completed ? "Completed" : "In Progress"}</p>
          <label>Assigned to:</label>
          <p>
            <select
              disabled={isAssigning}
              value={selectedUserId}
              onChange={async (event) => {
                try {
                  const userId = parseInt(event.target.value);
                  if (userId) {
                    setMessage("Assigning user");
                    setSelectedUserId(userId);
                    await assignTicket(userId);
                    setMessage("User assigned");
                  }
                } catch (error) {
                  console.error(
                    `User id needs to be a parsable string, instead it was: ${event.target.value}`
                  );
                }
              }}
            >
              <option key="" value="">
                -
              </option>
              {users.map((user) => {
                return (
                  <option key={user.id} value={user.id}>
                    {user.name}
                  </option>
                );
              })}
            </select>
          </p>
          {ticket?.completed ? null : (
            <button
              disabled={isCompleting}
              onClick={() => {
                completeTicket();
              }}
            >
              Complete
            </button>
          )}
          {message ? <p>{message}</p> : null}
        </>
      )}
    </div>
  );
};

export default TicketDetails;
